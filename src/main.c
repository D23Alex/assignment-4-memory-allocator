#define _DEFAULT_SOURCE
#include "mem.h"
#include <stdio.h>


void test__successful_memory_allocation() {
    heap_init(1);
    printf("[TEST] Successful memory allocation");

    printf("\nHeap before any allocation:\n");
    debug_heap(stdout, HEAP_START);

    void* allocated = _malloc(1024);
    printf("\nHeap after memory allocated:\n");
    debug_heap(stdout, HEAP_START);

    _free(allocated);
    printf("\nHeap after everything was freed:\n");
    debug_heap(stdout, HEAP_START);
    munmap(HEAP_START, 8192);
}

void test__allocate_several_blocks__free_one_block() {
    heap_init(1);
    printf("\n[TEST] Allocate several blocks and free one block");

    printf("\nHeap before any allocation:\n");
    debug_heap(stdout, HEAP_START);

    void* allocated1 = _malloc(512);
    void* allocated2 = _malloc(128);
    printf("\nHeap after all blocks were allocated:\n");
    debug_heap(stdout, HEAP_START);

    _free(allocated1);
    printf("\nHeap after one block was freed:\n");
    debug_heap(stdout, HEAP_START);
    _free(allocated2);
    printf("\nHeap after everything was freed:\n");
    debug_heap(stdout, HEAP_START);
    munmap(HEAP_START, 8192);
}

void test__allocate_several_blocks__free_two_blocks() {
    heap_init(1);
    printf("\n[TEST] Allocate several blocks and free two blocks");

    printf("\nHeap before any allocation:\n");
    debug_heap(stdout, HEAP_START);

    void* allocated1 = _malloc(512);
    void* allocated2 = _malloc(128);
    void* allocated3 = _malloc(4096);
    printf("\nHeap after all blocks were allocated:\n");
    debug_heap(stdout, HEAP_START);

    _free(allocated1);
    printf("\nHeap after one block was freed:\n");
    debug_heap(stdout, HEAP_START);
    _free(allocated2);
    printf("\nHeap after another block was freed:\n");
    debug_heap(stdout, HEAP_START);
    _free(allocated3);
    printf("\nHeap after everything was freed:\n");
    debug_heap(stdout, HEAP_START);
    munmap(HEAP_START, 8192);
}

void test__when_out_of_memory__allocated_region_extends_old() {
    heap_init(1);
    printf("[TEST] When out of memory, allocated region extends old");

    printf("\nHeap before any allocation:\n");
    debug_heap(stdout, HEAP_START);

    void* allocated = _malloc(65536);
    printf("\nHeap after memory allocated:\n");
    debug_heap(stdout, HEAP_START);

    _free(allocated);
    printf("\nHeap after everything was freed:\n");
    debug_heap(stdout, HEAP_START);
    munmap(HEAP_START, 95536);
}

void test__when_out_of_memory_and_cannot_extend_old_region__allocation_successful() {
    heap_init(1);
    printf("[TEST] When out of memory and cannot extend old region, allocation still successful");

    printf("\nHeap before any allocation:\n");
    debug_heap(stdout, HEAP_START);

    (void) mmap(HEAP_START + 8192, 4096, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS | MAP_FIXED_NOREPLACE, -1, 0);
    void* alloc1 = _malloc(123456);
    printf("\nHeap after memory allocated:\n");
    debug_heap(stdout, HEAP_START);

    _free(alloc1);
    printf("\nHeap after everything was freed:\n");
    debug_heap(stdout, HEAP_START);
}

int main() {
    test__successful_memory_allocation();
    test__allocate_several_blocks__free_one_block();
    test__allocate_several_blocks__free_two_blocks();
    test__when_out_of_memory__allocated_region_extends_old();
    test__when_out_of_memory_and_cannot_extend_old_region__allocation_successful();

    return 0;
}